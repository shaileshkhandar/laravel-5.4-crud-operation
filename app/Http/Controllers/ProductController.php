<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Http\Requests\ProductRequest;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $data = [];
        try{
            $data = Product::orderBy("id","desc")->get();
            $return =  view("product.view",compact("data"));
        }catch(\Exception $e){
            $return =  view("product.view",compact("data"));
        }
        return $return;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("product.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {   
        $input = $request->all();
    
        \DB::beginTransaction();
        try{

            if($input["hidden_id"] == ""){
                $product = New Product;
            }else{
                $product  = Product::find($input["hidden_id"]);
            }
            $product->name = $input["productname"];
            $product->sku = $input["sku"];
            $product->slug = str_slug($input["productname"], "-");
            $product->price = $input["price"];
            if($product){
                $product->save();
                \DB::commit();
                $return = redirect('product');
                $request->session()->flash('alert-success',"Record ".(isset($input["hidden_id"]) ? "Update" : "Add") .' successfully.!'); 
            }
        }catch(\Exception $e){
            \DB::rollback();
            $request->session()->flash('alert-success'," Problem to ".isset($input["hidden_id"]) ? "Update" : "Add"); 
            $return  = redirect('product/product'); 
        }
        return $return;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];
        try{
            $data = Product::find($id);
            $return = view("product.create",compact('data'));
        }catch(\Exception $e){
            $return = view("product.create",compact('data'));
        }

        return $return; 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        \DB::beginTransaction();
        try{
            Product::where("id",$id)->delete();
            $return =  redirect('product');
            $request->session()->flash('alert-success', 'Record Delete successfully.!');
            \DB::commit();
        }catch(\Exception $e){
            \DB::rollback();
            $request->session()->flash('alert-danger', 'problem to Record Delete.!');
            $return = redirect('product');
        }
        return $return;
    }
}
