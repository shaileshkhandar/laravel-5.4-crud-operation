<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Requests\CustomerRequest;

class CustomerController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $data = User::orderBy("id","desc")->get();
            $return = view("customer.view",compact('data'));
        }catch(\Exception $e){
            $return = view("customer.view",compact('data'));
        }
        return $return;
    }
    /*
     * create new form to register new customer
     * @request array
    */
    public function create()
    {
        return view('customer.create');
    }
    /*
     * create new required with validate data
     * @request array
    */
    public function store(CustomerRequest $request)
    {
        $input = $request->all();

        \DB::beginTransaction();
        try{

            if($input["hidden_id"] == ""){
                $user = New User;
            }else{
                $user = User::find($input["hidden_id"]);
            }
            if(array_key_exists("password",$input)){
                $user->password = bcrypt($input["password"]); 
            }
            $user->name =  $input["first_name"];
            $user->last_name =  $input["last_name"];
            $user->phone =  $input["phone"];
            $user->email =  $input["email"];
    
            if($user->save()){
                \DB::commit();
                $request->session()->flash('alert-success', 'Record ' .($input["hidden_id"] != "" ? ' Update ' : ' Add '). 'successful !');
                $return =  redirect("customer");
            }
        }catch(\Exception $e){
            \DB::rollback();
            $request->session()->flash('alert-danger', 'Problem to save record !');
            $return =  redirect("customer");
        }
        return $return;
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        try{
            $data = User::find($id);
            $return = view("customer.create",compact('data'));
            
        }catch(\Exception $e){
            $request->session()->flash('alert-danger', 'Error in get data!');
            $return = response("customer");
        }
        return $return;
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        \DB::beginTransaction();
        try{
            
            User::where("id",$id)->delete();
            
            $request->session()->flash('alert-success', 'User deleted successfully!');
            $return =  redirect('customer');
            \DB::commit();
        }catch(\Exception $e){
            
            $request->session()->flash('alert-danger', 'problem to deleted!');
            $return  = redirect('customer');
            \DB::rollback();
        }
        return $return;
    }
}