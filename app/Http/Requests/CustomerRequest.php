<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class CustomerRequest extends FormRequest 
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return TRUE;
    }
     /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request){
        $input = $request->all();
        if($input["hidden_id"] != ""){
            $rule = [
                "first_name"=>"required",
                "last_name"=>"required",
                "email"=>"required|Email|unique:users,email,".$input["hidden_id"].",id",
                "phone"=>"required|min:10|max:10|unique:users,phone,".$input["hidden_id"].",id",
                //"password"=>"required|min:6",
                "confirmpassword"=>"same:password",
            ];
        }else{
            $rule = [
                "first_name"=>"required",
                "last_name"=>"required",
                "email"=>"required|Email|unique:users,email",
                "phone"=>"required|min:10|max:10|unique:users,phone",
                "password"=>"required|min:6",
                "confirmpassword"=>"required|same:password",
            ];
        }
        return $rule;
    } 
}
