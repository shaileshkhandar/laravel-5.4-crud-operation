<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class ProductRequest extends FormRequest 
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return TRUE;
    }
     /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request){
        $input = $request->all();
        
        if($input["hidden_id"] != ""){
            $rule["productname"] = 'required|unique:products,name,' . $input['hidden_id'] . ',id';
        }else{
            $rule["productname"]= 'required|unique:products,name';
        }
        
        $rule = [
            "sku"=>"required",
            "price"=>"required|numeric"
        ];
        
        return $rule;
    } 
}
