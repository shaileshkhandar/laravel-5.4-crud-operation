@extends('layouts.app')

@section('content')

<div class="container">
  <h2>Product {{ isset($data) ? "Edit" : "Insert"}} From</h2>
  <form action="{{url("product")}}" method="post" >
    @csrf
    <div class="form-group">
      <label for="Productname">Productname:</label>
      <input type="Productname" class="form-control" id="Productname" placeholder="Enter Productname"  name="productname" value="{{ isset($data->name) ? $data->name : old('productname')}}">
      @if($errors->has("productname"))
          <div class="text-danger">{{ $errors->first('productname') }}</div>
      @endif
    </div>
    <div class="form-group">
      <label for="sku">Sku:</label>
      <input type="sku" class="form-control" id="sku" placeholder="Enter sku" name="sku" value="{{ isset($data->sku) ? $data->sku : old('sku')}}">
      @if($errors->has("sku"))
          <div class="text-danger">{{ $errors->first('sku') }}</div>
      @endif
    </div>
    <div class="form-group">
      <label for="price">Price:</label>
      <input type="price" class="form-control" id="price" placeholder="Enter price" name="price" value="{{ isset($data->price) ?$data->price :old('price')}}">
      @if($errors->has("price"))
          <div class="text-danger">{{ $errors->first('price') }}</div>
      @endif
    </div>
    <input type="hidden" name="hidden_id" value="{{isset($data) ? $data->id :""}}">
    <button type="submit" class="btn btn-primary">Submit</button>
    <a href="{{url("product")}}"><button type="button" class="btn btn-default">Cancel</button></a>
  </form>
</div>
@endsection