@extends('layouts.app')

@section('content')
<div class="container">
  <div class="flash-message">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
  
        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
      @endforeach
    </div> <!-- end .flash-message -->
  <a class="btn btn-primary float-right" href="{{url('product/create')}}">Add</a>
  <h2>Product Table</h2>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>ProductName</th>
        <th>Sku</th>
        <th>Price</th>
        <th colspan='2' class="text-center">Action</th>
      </tr>
    </thead>
    <tbody>
    @foreach($data as $list)
      <tr>
        <td>{{$list->name}}</td>
        <td>{{$list->sku}}</td>
        <td>{{$list->price}}</td>

        <td><a href="{{url('product/'.$list->id.'/edit')}}"><button class="btn btn-default">Edit</button></a></td>
        <td><form method="POST" action="{{ url('product', [$list->id]) }}">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <button type="submit" onclick="return confirm('Are you sure delete?')" class="btn btn-danger">Delete</button>
        </form></td>
      </tr>
      @endforeach
      @if(!count($data)) 
        <tr class="text-center"><td colspan="4">product not avaliable</td></tr>
      @endif
    </tbody>
  </table>
</div>
@endsection