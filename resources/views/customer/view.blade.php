@extends('layouts.app')

@section('content')
<div class="container">
  <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))

      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
  </div> <!-- end .flash-message -->
  <a class="btn btn-primary float-right" href="{{url('customer/create')}}">Add</a>
  <h2>Customer Table</h2>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>First name</th>
        <th>Last name</th>
        <th>Email</th>
        <th>Phone</th>
        <th colspan='2' class="text-center">Action</th>
      </tr>
    </thead>
    <tbody>
    @foreach($data as $list)
      <tr>
        <td>{{$list->name}}</td>
        <td>{{$list->last_name ? $list->last_name : $list->name }}</td>
        <td>{{$list->email}}</td>
        <td>{{$list->phone ? $list->phone : "-" }}</td>

        <td {{$list->id == 1 ? "colspan = 2 class=text-center" :"" }}><a href="{{url('customer/'.$list->id.'/edit')}}"><button class="btn btn-default">Edit</button></a></td>
        @if($list->id != 1)
          <td >
            <form method="POST" action="{{ url('customer', [$list->id]) }}">
              {{ csrf_field() }}
              {{ method_field('DELETE') }}
              <button type="submit" onclick="return confirm('Are you sure delete?')" class="btn btn-danger">Delete</button>
            </form>
          </td>
          @endif
      </tr>
      @endforeach 
    </tbody>
  </table>
</div>
@endsection