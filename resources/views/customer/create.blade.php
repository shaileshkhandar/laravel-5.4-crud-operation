@extends('layouts.app')

@section('content')
<div class="container">
  <h2>Customer {{ isset($data->id) ? "Edit" : "Insert" }}  Form</h2>
  <form action="{{url("customer")}}" method="POST">
    @csrf
    <div class="form-group">
      <label for="customer">FirstName:</label>
      <input type="customer" class="form-control" id="customer" placeholder="Enter FirstName" name="first_name" value="{{isset($data->name)  ? $data->name : old('first_name')}}">
      @if ($errors->has('first_name'))
          <div class="error text-danger">{{ $errors->first('first_name') }}</div>
      @endif
    </div>
    <div class="form-group">
      <label for="lastname">LastName:</label>
      <input type="lastname" class="form-control" id="lastname" placeholder="Enter LastName" name="last_name" value="{{isset($data->last_name) ? $data->last_name : old("last_name")}}">
      @if ($errors->has('last_name'))
          <div class="error text-danger">{{ $errors->first('last_name') }}</div>
      @endif
    </div>
    <div class="form-group">
      <label for="mobile">Mobile Number:</label>
      <input type="mobile" class="form-control" id="mobile" placeholder="Mobile Number" name="phone" value="{{ isset($data->phone) ? $data->phone : old("phone")}}">
      @if ($errors->has('phone'))
          <div class="error text-danger">{{ $errors->first('phone') }}</div>
      @endif
    </div>
    <div class="form-group">
      <label for="Email">Email:</label>
      <input type="Email" class="form-control" id="Email" placeholder="Enter Email" name="email" value="{{isset($data->email) ? $data->email : old("email")}}">
      @if ($errors->has('email'))
          <div class="error text-danger">{{ $errors->first('email') }}</div>
      @endif
    </div>
    <div class="form-group">
      <label for="Password">Password:</label>
      <input type="password" class="form-control" id="password" placeholder="Enter Password" name="password">
      @if ($errors->has('password'))
          <div class="error text-danger">{{ $errors->first('password') }}</div>
      @endif
    </div>
    <div class="form-group">
      <label for="ConfirmPassword">ConfirmPassword:</label>
      <input type="password" class="form-control" id="Confirmpassword" placeholder="Enter ConfirmPassword" name="confirmpassword">
      @if ($errors->has('confirmpassword'))
          <div class="error text-danger">{{ $errors->first('confirmpassword') }}</div>
      @endif
    </div>
    <input type="hidden" name="hidden_id" value="{{isset($data->id) ?$data->id:"" }}"> 
    <button type="submit" class="btn btn-primary">Submit</button>
    <a href="{{url("customer")}}"><button type="button" class="btn btn-default">Cancel</button></a>
  </form>
</div>
@endsection