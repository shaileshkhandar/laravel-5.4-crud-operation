<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(["middleware" => "auth"], function () {
    Route::get('/', function () {
        return view('welcome');
    });

    //route for manage home 
    Route::get('/home', 'HomeController@index')->name('home');

    //route for manage customers
    Route::get("customers",function(){return view("customer.view");});
    Route::resource("customer","CustomerController");

    //route for manage products
    Route::get("product",function(){return view("product.view"); });
    Route::resource("/product","ProductController");
});
Auth::routes();




